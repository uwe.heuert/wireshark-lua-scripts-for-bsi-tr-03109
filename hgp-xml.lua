-- 
-- Lua dissector for HGP/XML and CMS(HGP/XML)
--
-- Version 0.1.0
-- Last update: 2022.02.08
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- Declare the protocol
hgp_xml_proto = Proto("HgpXml", "HGP/XML")

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "0.1.0",
    author = "Uwe Heuert",
    description = "this dissector parses HGP/XML and CMS(HGP/XML)",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

-- Create a simple dissection function
function hgp_xml_proto.dissector(buffer, pinfo, tree)

    -- Create the HgpXml tree
    local t_hgp_xml = tree:add(hgp_xml_proto, buffer())
    local frame_len = buffer:len()
    local offset = 0

    local new_buffer = buffer(offset, frame_len)

    local firstByte = buffer(offset, 1):uint()
    if firstByte == 0x30 then
        pinfo.cols['protocol'] = "HTTP/CMS"
        local dissector = Dissector.get("cms2007")
		if dissector == nill then
			dissector = Dissector.get("cms")
		end
        dissector:call(new_buffer:tvb(), pinfo, t_hgp_xml)
        pinfo.cols['info'] = "POST / HTTP/1.1"
    else
        pinfo.cols['protocol'] = "HGP"
        local dissector = Dissector.get("xml")
        dissector:call(new_buffer:tvb(), pinfo, t_hgp_xml)
    end

    offset = offset + frame_len
end

function hgp_xml_proto.init()

    -- print("DissectorTable.list()")
    -- local list = DissectorTable.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    -- print("Dissector.list()")
    -- local list = Dissector.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

end

media_type_table = DissectorTable.get("media_type")
media_type_table:add("application/hgp", hgp_xml_proto)
