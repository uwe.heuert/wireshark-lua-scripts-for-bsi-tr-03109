# Plugins
LUA implementation of Wireshark dissector plugins for several protocols

****

## HDLC

Frame parsing:
- [x] I-Frame
- [x] S-Frame
- [x] U-Frame

****

## LMN SYM
Refer FNN and BSI TR-03109 documents.

****

## LMN Broadcast
Refer FNN and BSI TR-03109 documents.

****

## SML
Refer FNN and BSI TR-03109 documents.

****

## WAN Wakeup
Refer FNN and BSI TR-03109 documents.

****

## COSEM/XML
Refer FNN and BSI TR-03109 documents.

****

## SOCKS5 AuthTls
Refer FNN and BSI TR-03109 documents.

****