-- 
-- Lua dissector for SOCKS5 with AUTH TLS protocol
--
-- Version 0.1.0
-- Last update: 2022.01.19
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- Declare the protocol
socks5_proto = Proto("SOCKS5", "SOCKS5")

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "0.1.0",
    author = "Uwe Heuert",
    description = "this dissector parses SOCKS5",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

-- Declare the fields

-- SOCKS5 header
local version = ProtoField.uint8("socks.version", "Version", base.DEC)
local authmethod = ProtoField.uint8("socks.authmethod", "Accepted Auth Method", base.DEC)

local subversion = ProtoField.uint8("socks.subversion", "Subnegotiation version", base.DEC)
local subcommand = ProtoField.uint8("socks.subcommand", "SOCKS/TLS command", base.DEC)
local sublength = ProtoField.uint16("socks.subdatalength", "SOCKS/TLS data length", base.DEC)

socks5_proto.fields = {
    version, authmethod, subversion, subcommand, sublength
}

local state = 0;
local selectedAuthMethod = 0;

-- Create a simple dissection function
function socks5_proto.dissector(buffer, pinfo, tree)

    -- Create the SOCKS5 tree
    local t_socks5 = tree:add(socks5_proto, buffer())
    local frame_len = buffer:len()
    local offset = 0

    -- print("dissector called: " .. frame_len)

    local firstByte = buffer(offset, 1):uint()
    if firstByte == 5 then
        -- SOCKS5 standard
        if selectedAuthMethod ~= 0 and frame_len ~= 2 then
            -- reset states ... n-th turn
            state = 0
            selectedAuthMethod = 0
        end

        -- print("State: " .. state)
        if state == 0 then
            -- SOCKS5 authentication method request
            t_socks5:add(version, buffer(offset, 1))
            local version = buffer(offset, 1):uint()
            offset = offset + 1
            pinfo.cols['info'] = "Version: " .. version
    
            local count = buffer(offset, 1):uint()
            -- print("Count: " .. count)
            local t_authmethodsrequest = t_socks5:add(buffer(offset, count + 1), "Client Authentication Methods")
            t_authmethodsrequest:add(buffer(offset, 1), "Authentication Method Count:", count)
            offset = offset + 1
            for i = 1, count, 1 do
                local method = buffer(offset, 1):uint()
                local index = i - 1
                t_authmethodsrequest:add(buffer(offset, 1), "Method["..index.."]:", method)
                offset = offset + 1
            end

            state = state + 1
        elseif state == 1 then
            -- SOCKS5 authentication method response
            t_socks5:add(version, buffer(offset, 1))
            local version = buffer(offset, 1):uint()
            offset = offset + 1
            pinfo.cols['info'] = "Version: " .. version
    
            t_socks5:add(authmethod, buffer(offset, 1))
            selectedAuthMethod = buffer(offset, 1):uint()
            -- print("AuthMethod: " .. selectedAuthMethod)

            state = state + 1
        end        
    elseif firstByte == 1 then
        t_socks5:add(subversion, buffer(offset, 1))
        offset = offset + 1
        t_socks5:add(subcommand, buffer(offset, 1))
        offset = offset + 1
        t_socks5:add(sublength, buffer(offset, 2))
        local data_len = buffer(offset, 2):uint()
        offset = offset + 2

        pinfo.cols['info'] = "SOCKS/TLS"

        local new_buffer = buffer(offset, data_len)

        local dissector = Dissector.get("tls")
        dissector:call(new_buffer:tvb(), pinfo, tree)
        pinfo.cols['protocol'] = "TLS"
    end

    pinfo.cols['protocol'] = "Socks"

end

function socks5_proto.init()

    -- print("DissectorTable.list()")
    -- local list = DissectorTable.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    -- print("Dissector.list()")
    -- local list = Dissector.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    state = 0
end

-- load the tcp port table
tcp_table = DissectorTable.get("tcp.port")
-- register the protocol to port 1080
tcp_table:add(1080, socks5_proto)
