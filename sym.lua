-- 
-- Lua dissector for LMN SYM protocol
--
-- Version 0.1.0
-- Last update: 2022.01.31
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- Declare the protocol
sym_proto = Proto("SYM", "LMN SYM")

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "0.1.0",
    author = "Uwe Heuert",
    description = "this dissector parses LMN SYM",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

-- Declare properties editable in Wireshark UI
sym_proto.prefs.tool    = Pref.string("Decrypt Tool  ", "c:/Devel/es/svn/ts-libsharplmnsym/aescmac/bin/Debug/aescmac.exe", "Decrypt Tool AES/CMAC" )
sym_proto.prefs.id      = Pref.string("Meter ID       ", "0A0149545200034DE78E", "Meter ID" )
sym_proto.prefs.key     = Pref.string("Master Secret", "E76D1942BDAB119F1E6264D3FAE0B093", "Meter Master Key" )
sym_proto.prefs.decrypt = Pref.bool( "Decrypt Messages", false, "Execute decrypt tool" )

-- Declare the fields

-- SYM header
local command = ProtoField.uint8("sym.command", "Command", base.HEX)
local length = ProtoField.uint32("sym.length", "Length", base.DEC)
local counter = ProtoField.uint32("sym.counter", "Counter", base.HEX)
local encrypted = ProtoField.bytes("sym.encrypted", "Encrypted")
local mac = ProtoField.bytes("sym.mac", "MAC")
local decrypted = ProtoField.bytes("sym.decrypted", "Decrypted")
local version = ProtoField.uint32("sym.version", "Version", base.HEX)
local ciphersuite = ProtoField.uint16("sym.ciphersuite", "Ciphersuite", base.HEX)
local namedcurve = ProtoField.uint16("sym.namedcurve", "Namedcurve", base.HEX)
local x509 = ProtoField.bytes("sym.x509", "X.509")
local privkey = ProtoField.bytes("sym.privkey", "Privkey")
local fragmentlength = ProtoField.uint16("sym.fragmentlength", "Fragmentlength", base.DEC)

sym_proto.fields = {
    command, length, counter, encrypted, mac, decrypted, version, ciphersuite, namedcurve, x509, privkey, fragmentlength
}

-- Create a simple dissection function
function sym_proto.dissector(buffer, pinfo, tree)

    -- Create the sym tree
    local t_sym = tree:add(sym_proto, buffer())
    local frame_len = buffer:len()
    local offset = 0
    local com = 0
    local len = 0
    local buf_command
    local buf_length
    local buf_counter
    local buf_mac
    local buf_enc
    local buf_encmac
    local encrypted_length = 0

    t_sym:add(command, buffer(offset, 1))
    buf_command = buffer(offset, 1)
    com = buffer(offset, 1):uint()
	offset = offset + 1
    t_sym:add(length, buffer(offset, 4))
    buf_length = buffer(offset, 4)
    len = buffer(offset, 4):uint()
	offset = offset + 4
    t_sym:add(counter, buffer(offset, 4))
    buf_counter = buffer(offset, 4)
	offset = offset + 4
    buf_encmac = buffer(offset, len - offset)
    if len > 25 then
        encrypted_length = len - offset - 16
        t_sym:add(encrypted, buffer(offset, encrypted_length))
        buf_enc = buffer(offset, encrypted_length)
        offset = offset + encrypted_length
    end
    t_sym:add(mac, buffer(offset, 16))
    buf_mac = buffer(offset, 16)
	offset = offset + 16


    --ToDo decryption
    local tool = sym_proto.prefs.tool
    local exists = file_exists(tool)
    -- print(exists)
    if exists == true and sym_proto.prefs.decrypt == true then
        local k = ByteArray.new(sym_proto.prefs.key)
        local i = ByteArray.new(sym_proto.prefs.id)
        local t = buf_command:bytes()
        local l = buf_length:bytes()
        local c = buf_counter:bytes()
        local m = buf_mac:bytes()
        local cmd = tool.." -k "..k:tohex().." -i "..i:tohex().." -t "..t:tohex().." -l "..l:tohex().." -c "..c:tohex().." -m "..m:tohex()
        if encrypted_length > 0 then
            local e = buf_enc:bytes()
            cmd = cmd.." -e "..e:tohex()
        end

        -- print(cmd)
        local handle = io.popen(cmd)
        -- print(handle)
        local result = handle:read("*a")
        -- print(result)
        local t_decrypted = t_sym:add(buf_encmac, "[Decrypted]")
        local no = 0
        for s in result:gmatch("[^\r\n]+") do
            -- print(s)
            if no == 0 then
                t_decrypted:add(buf_mac, "CMAC:", s)
            elseif no == 1 then
                -- Create a new tab named "My Tvb" and add some data to it
                local b = ByteArray.new(s)
                local tvb = ByteArray.tvb(b, "My Tvb")

                -- Create a tree item that, when clicked, automatically shows the tab we just created
                local t_decryptedValue = t_decrypted:add( tvb(0, b:len()), "Data")
                t_decryptedValue:add(decrypted, tvb(0, b:len()))

                local decrypted_offset = 0
                if com == 0x82 then
                    t_decryptedValue:add(version, tvb(decrypted_offset, 4))
                    decrypted_offset = decrypted_offset + 4
                    local l = tvb(decrypted_offset, 1):uint()
                    decrypted_offset = decrypted_offset + 1
                    for i = 1, l do
                        t_decryptedValue:add(ciphersuite, tvb(decrypted_offset, 2))
                        decrypted_offset = decrypted_offset + 2
                    end
                    l = tvb(decrypted_offset, 1):uint()
                    decrypted_offset = decrypted_offset + 1
                    for i = 1, l do
                        t_decryptedValue:add(namedcurve, tvb(decrypted_offset, 2))
                        decrypted_offset = decrypted_offset + 2
                    end
                elseif com == 0x03 then
                    local l = tvb(decrypted_offset + 2, 2):uint()
                    l = l + 4
                    t_decryptedValue:add(x509, tvb(decrypted_offset, l))
                    decrypted_offset = decrypted_offset + l
                    l = b:len() - l
                    t_decryptedValue:add(privkey, tvb(decrypted_offset, l))
                    decrypted_offset = decrypted_offset + l
                elseif com == 0x04 then
                    t_decryptedValue:add(x509, tvb(decrypted_offset, b:len()))
                    decrypted_offset = decrypted_offset + b:len()
                elseif com == 0x85 then
                    t_decryptedValue:add(fragmentlength, tvb(decrypted_offset, 2))
                    decrypted_offset = decrypted_offset + 2
                end            
            end

            no = no + 1
        end

        handle:close()

    end

end

function file_exists(name)
    local f=io.open(name,"r")
    if f~=nil then io.close(f)
        return true
    else
        return false
    end
 end

function sym_proto.init()

    -- print("DissectorTable.list()")
    -- local list = DissectorTable.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    -- print("Dissector.list()")
    -- local list = Dissector.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    -- local command = "c:/Devel/es/svn/ts-libsharplmnsym/aescmac/bin/Debug/aescmac.exe -k E76D1942BDAB119F1E6264D3FAE0B093 -i 0A0149545200034DE78E -t 82 -l 00000039 -c 00000323 -e 77215608EDC0D68E538075F8155684E33592A971E197FA1366EB704F9E76083B -m 7E5EE9BDD39F4268E500B10AD321072D"
    -- local handle = io.popen(command)
    -- local result = handle:read("*a")
    -- handle:close()
    -- print(result)

end
