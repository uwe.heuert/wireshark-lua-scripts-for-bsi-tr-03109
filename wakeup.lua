-- 
-- Lua dissector for WakeUp packet
--
-- Version 0.1.0
-- Last update: 2022.01.25
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- Declare the protocol
wakeup_proto = Proto("WakeUp", "WakeUp over UDP")

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "0.1.0",
    author = "Uwe Heuert",
    description = "this dissector parses WakeUp over UDP",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

local messageType = ProtoField.uint16("wakeup.messageType", "Message type", base.HEX)
local version = ProtoField.uint8("wakeup.version", "Version", base.DEC)
local recipientIdLength = ProtoField.bytes("wakeup.recipientIdLen", "Recipient ID Length")
local recipientId = ProtoField.bytes("wakeup.recipientId", "Recipient ID")
local timestamp = ProtoField.bytes("wakeup.timestamp", "Timestamp")
local reserved = ProtoField.bytes("wakeup.reserved", "Reserved")
local signatureType = ProtoField.uint8("wakeup.signatureType", "Signature Type", base.DEC)
local signatureAlgorithmOIDLength = ProtoField.uint8("wakeup.signatureAlgorithmOIDLength", "signature Algorithm OID Length", base.DEC)
local signatureAlgorithmOID = ProtoField.bytes("wakeup.signatureAlgorithmOID", "Signature Algorithm OID")
local sigCertId = ProtoField.bytes("wakeup.sigCertId", "SigCert ID")
local ecDsaSignaturePartR = ProtoField.bytes("wakeup.ecDsaSignaturePartR", "ECDSA Signature Part R")
local ecDsaSignaturePartS = ProtoField.bytes("wakeup.ecDsaSignaturePartS", "ECDSA Signature Part S")

wakeup_proto.fields = {
    messageType,
    version,
    recipientIdLength,
    recipientId,
    timestamp,
    reserved,
    signatureType,
    signatureAlgorithmOIDLength,
    signatureAlgorithmOID,
    sigCertId,
    ecDsaSignaturePartR,
    ecDsaSignaturePartS
}

-- Create a simple dissection function
function wakeup_proto.dissector(buffer, pinfo, tree)

    -- Create the wakeup tree
    local t_wakeup = tree:add(wakeup_proto, buffer())
    local frame_len = buffer:len()
    local offset = 0

    -- Skip the uninterpreted wakeup header data
    -- t_wakeup:add(buffer(offset, frame_len), "Header:", "Uninterpreted Data Sequence ("..frame_len.." bytes)")
    -- offset = offset + frame_len

    t_wakeup:add(messageType, buffer(offset, 2))
    offset = offset + 2
    t_wakeup:add(version, buffer(offset, 1))
    local ver = buffer(offset, 1):uint()
    offset = offset + 1
    if ver == 1 then
        t_wakeup:add(recipientId, buffer(offset, 9))
        offset = offset + 9
    elseif ver == 2 then
        t_wakeup:add(recipientIdLength, buffer(offset, 1))
        offset = offset + 1
        t_wakeup:add(recipientId, buffer(offset, 20))
        offset = offset + 20
    end
    t_wakeup:add(timestamp, buffer(offset, 8))
    offset = offset + 8
    if ver == 1 then
        t_wakeup:add(reserved, buffer(offset, 12))
        offset = offset + 12        
    end
    t_wakeup:add(signatureType, buffer(offset, 1))
    local sigType = buffer(offset, 1):uint()
    offset = offset + 1
    local sigLen
    if ver == 1 then
        sigLen = 64
        t_wakeup:add(signatureAlgorithmOIDLength, buffer(offset, 1))
        local len = buffer(offset, 1):uint()
        offset = offset + 1
        t_wakeup:add(signatureAlgorithmOID, buffer(offset, len))
        offset = offset + len
    elseif ver == 2 then
        if sigType == 1 then
            sigLen = 64
        elseif sigType == 2 then
            sigLen = 96
        elseif sigType == 3 then
            sigLen = 128
        end
        t_wakeup:add(reserved, buffer(offset, 11))
        offset = offset + 11
        t_wakeup:add(sigCertId, buffer(offset, 20))
        offset = offset + 20
    end
    t_wakeup:add(ecDsaSignaturePartR, buffer(offset, sigLen/2))
    offset = offset + sigLen/2
    t_wakeup:add(ecDsaSignaturePartS, buffer(offset, sigLen/2))
    offset = offset + sigLen/2

end

-- load the udp port table
udp_table = DissectorTable.get("udp.port")
-- register the protocol to ports 5025, 4063, 4059, 40000 (should be changed based on your data)
udp_table:add(15951, wakeup_proto)
