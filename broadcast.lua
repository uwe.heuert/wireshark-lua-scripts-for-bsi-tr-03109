-- 
-- Lua dissector for LMN BROADCAST protocol
--
-- Version 0.1.0
-- Last update: 2022.01.25
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- Declare the protocol
broadcast_proto = Proto("BROADCAST", "LMN BROADCAST")

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "0.1.0",
    author = "Uwe Heuert",
    description = "this dissector parses LMN BROADCAST",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

-- Declare the fields

-- BROADCAST header
local address = ProtoField.uint8("broadcast.address", "Address", base.HEX)
local timeslot = ProtoField.uint8("broadcast.timeslot", "Timeslot", base.DEC)
local lmn_id = ProtoField.bytes("broadcast.lmn_id", "LMN ID")
local reserved1 = ProtoField.bytes("broadcast.reserved1", "Reserved1")
local sensor_id = ProtoField.bytes("broadcast.sensor_id", "Sensor ID")
local reserved2 = ProtoField.bytes("broadcast.reserved2", "Reserved2")
local correlstate = ProtoField.uint16("broadcast.correlstate", "Correlstate", base.HEX)

broadcast_proto.fields = {
    address,
    timeslot,
    lmn_id,
    reserved1,
    sensor_id,
    reserved2,
    correlstate
}

-- Create a simple dissection function
function broadcast_proto.dissector(buffer, pinfo, tree)

    -- Create the broadcast tree
    local t_broadcast = tree:add(broadcast_proto, buffer())
    local frame_len = buffer:len()
    local offset = 0
    local number = frame_len / 32

    for i = 0, number - 1 do
        -- parsing of structure
        local t_device = t_broadcast:add(buffer(offset, 32), "Device:", i)
        t_device:add(address, buffer(offset, 1))
        offset = offset + 1
        t_device:add(timeslot, buffer(offset, 1))
        offset = offset + 1
        t_device:add(lmn_id, buffer(offset, 10))
        offset = offset + 10
        t_device:add(reserved1, buffer(offset, 4))
        offset = offset + 4
        t_device:add(sensor_id, buffer(offset, 10))
        offset = offset + 10
        t_device:add(reserved2, buffer(offset, 4))
        offset = offset + 4
        t_device:add(correlstate, buffer(offset, 2))
        offset = offset + 2
    end

end
