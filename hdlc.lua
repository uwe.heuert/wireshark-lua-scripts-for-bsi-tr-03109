-- 
-- Lua dissector for HDLC encapsulated over TCP (used for SCADA systems over TCP/IP)
-- The payload is interpreted as a DLMS/COSEM message (requires dlms.lua dissector)
--
-- Version 1.1.1
-- Last update: 2022.02.02
--
-- (c) Petr Matousek, FIT BUT, Czech Republic, 2018
-- Contact:  matousp@fit.vutbr.cz
-- Developed as a part of IRONSTONE research project
--
-- (c) Uwe Heuert, exceeding solutions, Germany, 2022
-- Contact:  uwe.heuert@exceeding-solutions.de
--

-- declare the protocol
-- local proto_desc = "HDLC over TCP"
local proto_desc = "HDLC"
hdlc_proto = Proto("HDLC", proto_desc)

-- Display version number for Lua dissector
local dissector_info = 
{
    version = "1.1.1",
    author = "Uwe Heuert",
    description = "this dissector parses HDLC",
    repository = "https://www.exceeding-solutions.de"
}
set_plugin_info(dissector_info)

-- Declare properties editable in Wireshark UI
hdlc_proto.prefs.lmn = Pref.bool( "LMN", false, "German FNN LMN address interpretation" )

-- declare the value strings
local FrameFormatTypeVALS = {
    [0xA] = "Number 3",
}

local SegVALS = {
    [0] = "Not Set",
    [1] = "Set",
}

local FrameTypeVALS = {
    [0] = "I-Frame",
    [1] = "S-Frame",
    [3] = "U-Frame",
}

local sFrameTypeVALS = {
    [0] = "Receive Ready (RR)",
    [1] = "Receive Not Read (RNR)",
    [2] = "Reject (REJ)",
    [3] = "Selective Reject (SREJ)",
}

local uFrameTypeVALS = {
    [0] = "Unnumbered Information (UI)",
    [1] = "Request Init.Mode (RIM)",                -- SIM
    [3] = "Disconnected Mode (DM)",                 -- SARM
    [7] = "Set Async. Balanced Mode (SABM)",
    [8] = "Unnumbered Poll (UP)",
    [16] = "Disconnect (DISC)",                     -- RD
    [24] = "Unnumbered Ack (UA)",
    [32] = "Set Normal Response Mode (SNRM)",
    [33] = "Frame Reject (FRMR)",                   -- CMDR
    [39] = "Exchange Identification (XID)",
}

local LLCsrcSAPVALS = {[0xe6] = "Command", [0xe7] = "Response"}

-- Declare the fields

-- HDLC header
local flag = ProtoField.uint8("hdlc.flag", "Flag", base.HEX)
local frameFormat = ProtoField.uint16("hdlc.frameFormat", "Frame Format", base.HEX)
local frameFormatType = ProtoField.uint16("hdlc.frameType", "HDLC Frame Format Type", base.DEC, FrameFormatTypeVALS, 0xf000)
local segmentationFlag = ProtoField.uint16("hdlc.segmentationFlag", "Segmentation Flag", base.DEC, SegVALS, 0x0800)
local frameLen = ProtoField.uint16("hdlc.frameLen", "HDLC Frame Length", base.DEC, nil, 0x07FF)
local dstAddress = ProtoField.uint32("hdlc.dstAddress", "Destination Address", base.HEX)
local srcAddress = ProtoField.uint32("hdlc.srcAddress", "Source Address", base.HEX)
local ctrlField = ProtoField.uint8("hdlc.ctrlField", "Control Field", base.HEX)

-- HDLC frame types
local frameType = ProtoField.uint8("hdlc.frameType", "Frame type", base.HEX, FrameTypeVALS, 0x01)
local polling = ProtoField.uint8("hdlc.polling", "P/F flag", base.DEC, nil, 0x10)
local recvNumber = ProtoField.uint8("hdlc.recvNumber", "N(R)", base.DEC, nil, 0xe0)

-- I-Frames
local sentNumber = ProtoField.uint8("hdlc.sentNumber", "N(S)", base.DEC, nil, 0x0e)

-- S-Frames
local sUnused = ProtoField.uint8("hdlc.sUnused", "Unused", base.DEC, nil, 0x02)
local sFrameType = ProtoField.uint8("hdlc.sFrameType", "Type", base.DEC, sFrameTypeVALS, 0x0c)

-- U-Frames
local frameType2 = ProtoField.uint8("hdlc.frameType", "Frame type", base.HEX, FrameTypeVALS, 0x03)
local uFrameType = ProtoField.uint8("hdlc.uFrameType", "Type", base.DEC, uFrameTypeVALS, 0xec)

-- HDLC data and trailer
local HCS = ProtoField.uint16("hdlc.HCS", "Header Checksum", base.HEX)
local FCS = ProtoField.uint16("hdlc.FCS", "Frame Checksum", base.HEX)

-- LLC layer header
-- local LLC_type = ProtoField.uint16("hdlc.llc_type","LLC command/response", base.HEX, LLCTypeVALS)
local LLC_dstSAP = ProtoField.uint8("hdlc.llc_dstSAP", "Destination LSAP", base.HEX)
local LLC_srcSAP = ProtoField.uint8("hdlc.llc_srcSAP", "Source LSAP", base.HEX, LLCsrcSAPVALS)
local LLC_control = ProtoField.uint8("hdlc.llc_control", "LLC Quality")
local LLC_data = ProtoField.bytes("hdlc.llc_data", "Data")

hdlc_proto.fields = {
    flag, frameFormat, frameFormatType, segmentationFlag, frameLen, dstAddress, srcAddress, ctrlField,
    frameType, polling, recvNumber, sentNumber, sUnused, sFrameType, frameType2, uFrameType,
    HCS, LLC_dstSAP, LLC_srcSAP, LLC_control, LLC_data, FCS
}

-- create the dissection function
function hdlc_proto.dissector(buffer, pinfo, tree)

    -- Set the protocol column
    --print(hdlc_proto.prefs.lmn)

    -- create the HDLC protocol tree item
    local t_hdlc = tree:add(hdlc_proto, buffer())
    local offset = 0
	local frame_len = 0
	local header_len = 0
    local information_len = 0
    local hdlc_flag = buffer(offset, 1):uint()

    if hdlc_flag == 0x7e then -- it is a HDLC frame
        t_hdlc:add(flag, buffer(offset, 1))

        local t_frameFormat = t_hdlc:add(frameFormat, buffer(offset + 1, 2))
        t_frameFormat:add(frameFormatType, buffer(offset + 1, 2))
        t_frameFormat:add(segmentationFlag, buffer(offset + 1, 2))
		t_frameFormat:add(frameLen, buffer(offset + 1, 2))
		
		-- moving offset to the destination address
		offset = offset + 3
		local addr_len = 1
		for i = 0, 3 do
			local addr_byte = buffer(offset + i, 1):uint()
			if bit32.band(addr_byte, 0x01) == 0x01 then
				break
			end
			addr_len = addr_len + 1
		end
		t_hdlc:add(dstAddress, buffer(offset, addr_len))
        local dstAdd = buffer(offset, addr_len):uint()
        local dstAdd1 = buffer(offset, 1):uint()
        local dstAdd2 = 0
        if addr_len > 1 then
            dstAdd2= buffer(offset + 1, 1):uint()
        end
        if hdlc_proto.prefs.lmn then
            if dstAdd1 == 0x02 then
                pinfo.cols['dst'] = string.format('SMGW (%02X)', dstAdd1)
            elseif dstAdd1 == 0xFE then
                pinfo.cols['dst'] = string.format('BROADCAST (%02X)', dstAdd1)
            else
                pinfo.cols['dst'] = string.format('METER (%02X)', dstAdd1)
            end
        else
            pinfo.cols['dst'] = string.format('%02X', dstAdd)
        end
        
		-- moving offset to the source address
		offset = offset + addr_len
		local addr_len = 1
		for i = 0, 3 do
			local addr_byte = buffer(offset + i, 1):uint()
			if bit32.band(addr_byte, 0x01) == 0x01 then
				break
			end
			addr_len = addr_len + 1
		end
        t_hdlc:add(srcAddress, buffer(offset, addr_len))
        local srcAdd = buffer(offset, addr_len):uint()
        local srcAdd1 = buffer(offset, 1):uint()
        local srcAdd2 = 0
        if addr_len > 1 then
            srcAdd2= buffer(offset + 1, 1):uint()
        end
        if hdlc_proto.prefs.lmn then
            if srcAdd1 == 0x02 then
                pinfo.cols['src'] = string.format('SMGW (%02X)', srcAdd1)
            else
                pinfo.cols['src'] = string.format('METER (%02X)', srcAdd1)
            end
        else
            pinfo.cols['src'] = string.format('%02X', srcAdd)
        end

        -- moving offset to the control field
		offset = offset + addr_len
		local t_ctrlField = t_hdlc:add(ctrlField, buffer(offset, 1))

        local frameCode = buffer:range(offset, 1)
		local frameBits = frameCode:bitfield(6, 2)
		local recv = 0
		local sent = 0
        local sType = 0
        local uType = 0

        if frameBits == 0 or frameBits == 2 then
            -- iFrames structure
            t_ctrlField:add(recvNumber, buffer(offset, 1))
            t_ctrlField:add(polling, buffer(offset, 1))
            t_ctrlField:add(sentNumber, buffer(offset, 1))
            t_ctrlField:add(frameType, buffer(offset, 1))
            frameBits = frameCode:bitfield(7, 1)
            recv = frameCode:bitfield(0, 3)
            sent = frameCode:bitfield(4, 3)
        elseif frameBits == 1 then
            -- sFrames structure
            t_ctrlField:add(recvNumber, buffer(offset, 1))
            t_ctrlField:add(polling, buffer(offset, 1))
            t_ctrlField:add(sFrameType, buffer(offset, 1))
            t_ctrlField:add(sUnused, buffer(offset, 1))
            t_ctrlField:add(frameType, buffer(offset, 1))
            frameBits = frameCode:bitfield(7, 1)
            recv = frameCode:bitfield(0, 3)
            sType = frameCode:bitfield(4, 2)
        elseif frameBits == 3 then
            -- uFrames structure
            t_ctrlField:add(uFrameType, buffer(offset, 1))
            t_ctrlField:add(polling, buffer(offset, 1))
            t_ctrlField:add(frameType2, buffer(offset, 1))
            frameBits = frameCode:bitfield(6, 2)
            uType = frameCode:bitfield(4, 2)
            uType = uType + frameCode:bitfield(0, 3) * 8
        end

		-- moving offset to the next field
		offset = offset + 1

		--print(FrameBits)

        -- iFrames structure
        if frameBits == 0 then
            -- the length of the frame including opening and closing flags
            frame_len = buffer:len()
            -- the length of the information field
            information_len = frame_len - (offset + 3)

            if information_len > 0 then -- there is a non-empty information field
                -- HCS field
                t_hdlc:add(HCS, buffer(offset, 2))
                information_len = information_len - 2 -- length without HCS field
                local destSAP = buffer(offset + 2, 1):uint()
                offset = offset + 2
                if destSAP == 0xe6 then -- testing header LLC presence

                    local t_LLC_frame = t_hdlc:add(buffer(offset, information_len), "Information")
                    t_LLC_frame:add(LLC_dstSAP, buffer(offset, 1))
                    t_LLC_frame:add(LLC_srcSAP, buffer(offset + 1, 1))

                    t_LLC_frame:add(LLC_control, buffer(offset + 2, 1))
                    t_LLC_frame:add(LLC_data, buffer(offset + 3, information_len - 3))

                    local new_buffer = buffer(offset + 3, information_len - 3)

                    local dissector = Dissector.get("dlms")
                    dissector:call(new_buffer:tvb(), pinfo, tree)

                    offset = offset + information_len

                    pinfo.cols['protocol'] = "DLMS"
                else -- no LLC header present but non-empty data field
                    local dataLen = "Data (" .. information_len .. " bytes)"
                    t_hdlc:add(buffer(offset, information_len), dataLen)
                    pinfo.cols['protocol'] = proto_desc
					
                    -- look here: Dealing with segmented data in a Wireshark
                    -- https://tewarid.github.io/2013/05/21/dealing-with-segmented-data-in-a-wireshark-dissector-written-in-lua.html

                    local new_buffer = buffer(offset, information_len)

                    if hdlc_proto.prefs.lmn then

                        pinfo.cols['info'] = "HDLC LMN (" .. information_len .. " bytes)"

                        local protSel = dstAdd2
                        --print(protSel)
                        if protSel == 0x0d then
                            local dissector = Dissector.get("sym")
                            dissector:call(new_buffer:tvb(), pinfo, tree)
                            pinfo.cols['protocol'] = "SYM"
                        elseif protSel == 0x03 then
                            local dissector = Dissector.get("tls")
                            dissector:call(new_buffer:tvb(), pinfo, tree)
                            pinfo.cols['protocol'] = "TLS"
                        elseif protSel == 0x07 then
                            local dissector = Dissector.get("sml105")
							if dissector == nill then
							    dissector = Dissector.get("sml")
							end
                            dissector:call(new_buffer:tvb(), pinfo, tree)
                            pinfo.cols['protocol'] = "SML"
                        end
                    else
                        pinfo.cols['info'] = "HDLC segmented data (" .. information_len .. " bytes)"

                        local dissector = Dissector.get("dlms")
                        --dissector:call(new_buffer:tvb(), pinfo, tree)
                    end

                    offset = offset + information_len
                end
            else -- I-frame has an empty information field
                pinfo.cols['protocol'] = proto_desc
                pinfo.cols['info'] = FrameTypeVALS[0] .. ", no data"
            end
        elseif frameBits == 1 then
            -- sFrames  struture
            pinfo.cols['protocol'] = proto_desc
            pinfo.cols['info'] = FrameTypeVALS[1] .. ", " .. sFrameTypeVALS[sType] .. ": N(R)=" .. recv
		elseif frameBits == 3 then
            -- uFrames struture
            pinfo.cols['protocol'] = proto_desc
            -- the length of the frame including opening and closing flags
            frame_len = buffer:len()
            -- the length of the information field
            information_len = frame_len - (offset + 3)

            if information_len > 0 then -- there is a non-empty information field
                -- HCS field
                t_hdlc:add(HCS, buffer(offset, 2))
                information_len = information_len - 2 -- length without HCS field

                offset = offset + 2

				local dataLen = "Data (" .. information_len .. " bytes)"
                t_hdlc:add(buffer(offset, information_len), dataLen)
					
                local new_buffer = buffer(offset, information_len)

                if hdlc_proto.prefs.lmn then
                    -- pinfo.cols['info'] = "HDLC LMN (" .. information_len .. " bytes)"
                    pinfo.cols['info'] = FrameTypeVALS[3] .. ", " .. uFrameTypeVALS[uType]

                    local protSel = dstAdd2
                    --print(protSel)
                    if protSel == 0x03 then
                        -- dissect broadcast 1
                        local dissector = Dissector.get("broadcast")
                        dissector:call(new_buffer:tvb(), pinfo, tree)
                        pinfo.cols['protocol'] = "BC1"
                    elseif protSel == 0x05 then
                        -- dissect broadcast 2
                        local dissector = Dissector.get("broadcast")
                        dissector:call(new_buffer:tvb(), pinfo, tree)
                        pinfo.cols['protocol'] = "BC2"
                    end
                else
                    pinfo.cols['info'] = "HDLC segmented data (" .. information_len .. " bytes)"

                    local dissector = Dissector.get("dlms")
                    --dissector:call(new_buffer:tvb(), pinfo, tree)
                end


                offset = offset + information_len
			else
                pinfo.cols['info'] = FrameTypeVALS[3] .. ", " .. uFrameTypeVALS[uType]
			end
        end

        t_hdlc:add(FCS, buffer(offset, 2))
        t_hdlc:add(flag, buffer(offset + 2, 1))
    end

end

function hdlc_proto.init()

    -- print("DissectorTable.list()")
    -- local list = DissectorTable.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

    -- print("Dissector.list()")
    -- local list = Dissector.list()
    -- for i,line in ipairs(list) do
    --     print(line)
    -- end
    -- print()

end

-- load the tcp port table
tcp_table = DissectorTable.get("tcp.port")
-- register the protocol to port 4061 and 4060
tcp_table:add(4061, hdlc_proto)
tcp_table:add(4060, hdlc_proto)
